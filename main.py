#!/usr/bin/env python3

import pdb
import sys
import subprocess
from phabricator import Phabricator

KEY = 'Ep8ScSKaNBE.'
ONE_YEAR_AGO = int(subprocess.check_output([
    'date',
    '--date',
    '1 year ago',
    '+%s',
]).decode('utf8'))

def chunks(lst, n):
    """
    Yield successive n-sized chunks from lst.
    <https://stackoverflow.com/a/312464>
    """
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

class RelengTeam():
    def __init__(self, phab):
        self.phab = phab
        self.tasks = []
        self.projects = {}
        self.task_projects = {}
        self.phids = set()

    def get_tasks(self, after:str=None) -> list:
        args = {
            'queryKey': KEY,
            'attachments': {
                'projects': True
            },
            'constraints': {
                'createdStart': ONE_YEAR_AGO,
            }
        }
        if after is not None:
            args['after'] = after
        data = self.phab.maniphest.search(**args)
        self.tasks += data['data']
        print(len(self.tasks), file=sys.stderr)
        after = data['cursor']['after']
        if after is not None:
            return self.get_tasks(after=after)
        return self.tasks

    def get_transactions(self):
        if not self.tasks:
            return
        task_ids = [x['id'] for x in self.tasks]
        for tasks_ids_chunk in chunks(task_ids, 100):
            data = self.phab.maniphest.gettasktransactions(ids=tasks_ids_chunk)
            for task_id, trans in data.items():
                self.task_projects[task_id] = [
                    item for sublist in [
                        x['oldValue'] + x['newValue']
                        for x in trans
                        if x['transactionType'] == 'core:edge'
                        and x['meta']['edge:type'] == 41
                    ] for item in sublist
                ]
            print(len(self.task_projects), file=sys.stderr)

    def get_phids(self) -> set:
        if not self.tasks:
            return set()

        if self.phids:
            return self.phids

        for task in self.tasks:
            task['attachments']['projects']['projectPHIDs'] += self.task_projects.get(str(task['id']), [])
            self.phids = self.phids.union(set(task['attachments']['projects']['projectPHIDs']))

        return self.phids

    def get_projects(self, after:str=None):
        if not self.tasks:
            return {}

        args = {
            'constraints': {
                'phids': list(self.get_phids())
            }
        }
        if after is not None:
            args['after'] = after

        data = self.phab.project.search(**args)
        for proj in data['data']:
            self.projects[proj['phid']] = proj['fields']['name']
        print(len(self.projects), file=sys.stderr)
        after = data['cursor']['after']
        if after is not None:
            return self.get_projects(after=after)
        return self.projects

    def tasks_touching(self, project):
        ret = []
        for task in self.tasks:
            for proj in task['attachments']['projects']['projectPHIDs']:
                if self.projects[proj].startswith(project):
                    ret.append(task)

        return ret


ret = RelengTeam(Phabricator())
print('== Tasks (for past year) ==', file=sys.stderr)
ret.get_tasks()
print('== Transactions ==', file=sys.stderr)
ret.get_transactions()
print('== Projects ==', file=sys.stderr)
ret.get_projects()
sprint_name = sys.argv[1]
sprint_tasks = ret.tasks_touching(sprint_name)
total_points = 0
resolved_points = 0
numopen = 0
numclosed = 0
seen_ids = []
for task in sprint_tasks:
    task_id = task['id']
    task_status = task['fields']['status']['name']

    if task_id in seen_ids:
        continue

    seen_ids.append(task_id)

    task_points = task['fields'].get('points')
    if task_points is None:
        task_points = 1
    task_points = int(task_points)

    total_points += task_points

    print(f'https://phabricator.wikimedia.org/T{task_id} ({task_status}): {task_points} points', file=sys.stderr)

    if task_status == 'Open':
        numopen+=1
    if task_status == 'Resolved':
        numclosed+=1
        resolved_points += task_points

print(f'''
* * *

```
python3 main.py '{sprint_name}' >> README.md
```

## Sprint: "{sprint_name}" summary

* Total tasks: {len(seen_ids)}
* Total points (1 point for each task without points assigned): {total_points}
* Still open: {numopen}
* Closed: {numclosed}
* Resolved points (1 point for each task without points assigned): {resolved_points}
''')
