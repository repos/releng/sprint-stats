Releng Sprint Tools
===================

This project provides tools to track RelEng's sprint progress

* * *

```
python3 main.py 'Done by Wed 06 Oct' >> README.md
```

## Sprint: "Done by Wed 06 Oct" summary

* Total tasks: 16
* Total points (1 point for each task without points assigned): 38
* Still open: 11
* Closed: 4
* Resolved points (1 point for each task without points assigned): 11


* * *

```
python3 main.py 'Done by Thu 04 Nov' >> README.md
```

## Sprint: "Done by Thu 04 Nov" summary

* Total tasks: 25
* Total points (1 point for each task without points assigned): 50
* Still open: 9
* Closed: 12
* Resolved points (1 point for each task without points assigned): 28

* * *

```
python3 main.py 'Done by Wed 24 Nov' >> README.md
```

## Sprint: "Done by Wed 24 Nov" summary

* Total tasks: 24
* Total points (1 point for each task without points assigned): 72
* Still open: 8
* Closed: 11
* Resolved points (1 point for each task without points assigned): 33

(**NOTE**: Took an extra week to run stats on ^)

* * *

```
python3 main.py 'Done by Feb 23' >> README.md
```

## Sprint: "Done by Feb 23" summary

* Total tasks: 20
* Total points (1 point for each task without points assigned): 59
* Still open: 8
* Closed: 9
* Resolved points (1 point for each task without points assigned): 26


* * *

```
python3 main.py 'Bonus Level 🕹️' >> README.md
```

## Sprint: "Bonus Level 🕹️" summary

* Total tasks: 27
* Total points (1 point for each task without points assigned): 56
* Still open: 13
* Closed: 10
* Resolved points (1 point for each task without points assigned): 15

